# Docker контейнер для linux 1С клиента

Контейнер на базе ubuntu 14.04, в составе infinality-патчи (рендеринг шрифтов),
шрифт "Fira Code", набор стандартных шрифтов mscorefonts (читайте лицензию в архиве),
набор тем Zukitwo, UltraFlatIcons, YltraIcons


### Настройка, сборка и использование

* В Dockerfile исправить версию платформы, архитектуру и используемую кодировку
```
  ENV PLAT_VERSION 8.3.7-1873 <br>
  ENV PLAT_ARCH amd64 <br>
  ENV LANG ru_RU.utf8 <br>
```

* Разместить официальные deb-пакеты 1С соответствующих версий и архитектуры (common, server, client) в директории ./dist 

* Собрать контейнер (где 1469 ветка платформы):

 ```docker build -t ep4sh/1c-8312:1469 .```

* Использовать для запуска ```./run.sh``` или комманду вида

```docker run -d -t --name=1c -e DISPLAY -v $HOME/.Xauthority:/home/user/.Xauthority -v $HOME:/home/user -v /mnt:/mnt --net=host --pid=host --ipc=host ep4sh/1c-8312:1469```

* Вывалить юзеру ярлык на рабочий стол (`run.sh`):

```
#!/bin/bash
docker exec -it  1c /opt/1C/v8.3/x86_64/1cv8 &> /dev/null &
```

#### Авторство
[ep4sh2k](https://gitlab.com/ep4sh) 

#### Оригинальный репозиторий
[psyriccio (c) ](https://github.com/psyriccio/dck1c)  
Огромное Вам спасибо.   


#### Документированные изменения: [changes](./CHANGES.md) 

#### Лицензия: [GNU LGPL](./LICENCE)

