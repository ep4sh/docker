 * Убраны директории:
  - light
  - screenshots

* Удалены файлы:
 - start.sh

Изменения в файлах:

### run.sh
```
--- /home/ep4sh/Downloads/dck1c-release/run.sh
+++ /home/ep4sh/1cDC/run.sh
@@ -3,4 +3,4 @@
 mkdir /tmp/user
 cp -R ~/.1C /tmp/user
 cp -R ~/.1cv8 /tmp/user
-docker run -it --rm -e DISPLAY -v $HOME/.Xauthority:/home/user/.Xauthority -v $HOME:/home/user -v /mnt:/mnt --net=host --pid=host --ipc=host psyriccio/dck1c $*
+docker run --rm -d -t --name=1c -e DISPLAY -v $HOME/.Xauthority:/home/user/.Xauthority -v $HOME:/home/user -v /mnt/docker:/mnt --net=host --pid=host --ipc=host ep4sh/1c-8312:1469
```



### Dockerfile
```
--- /home/ep4sh/1cDC/Dockerfile
+++ /home/ep4sh/Downloads/dck1c-release/Dockerfile
@@ -1,22 +1,22 @@
 FROM ubuntu:14.04
+MAINTAINER ep4sh
-MAINTAINER psyriccio
 
 ENV DEBIAN_FRONTEND noninteractive
 
 RUN apt-get update \
+      && apt-get install -y software-properties-common  \
-      && apt-get install -y software-properties-common python-software-properties \
       && add-apt-repository multiverse && add-apt-repository ppa:no1wantdthisname/ppa && add-apt-repository ppa:openjdk-r/ppa && apt-get update && apt-get upgrade -y \
+      && apt-get install -y unixodbc imagemagick libglib2.0-dev libt1-5 t1utils openjdk-8-jdk \
-      && apt-get install -y unixodbc libgsf-1-114 imagemagick libglib2.0-dev libt1-5 t1utils openjdk-8-jdk \
           libwebkit-dev libcanberra-gtk-module unzip fontconfig-infinality gtk2-engines-murrine gtk2-engines-pixbuf dialog nano \
       && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
+	    && localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8 \
+      && apt-get update && apt-get install -y libwebkitgtk-3.0-0 libgsf-1-114 
-	    && localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8
 
+ENV PLAT_VERSION 8.3.12-1469
+ENV PLAT_ARCH amd64
-ENV PLT_VERSION 8.3.7-1873
-ENV PLT_ARCH amd64
 ENV LANG ru_RU.utf8
 
 ADD ./dist/ /opt/
-ADD ./start.sh /opt/
 
 RUN dpkg -i /opt/1c-enterprise83-common_${PLT_VERSION}_${PLT_ARCH}.deb \
             /opt/1c-enterprise83-server_${PLT_VERSION}_${PLT_ARCH}.deb \
@@ -27,7 +27,7 @@
       && unzip /opt/zukitwo-themes.zip -d /usr/share/themes \
       && unzip /opt/yltra-icons.zip -d /usr/share/icons \
       && unzip /opt/ultraflat-icons.zip -d /usr/share/icons \
+      && rm /opt/*.deb && rm /opt/*.zip \ 
-      && rm /opt/*.deb && rm /opt/*.zip && chmod +x /opt/start.sh \
       && /bin/bash /etc/fonts/infinality/infctl.sh setstyle linux
 
 RUN export uid=1000 gid=1000 && \
@@ -40,3 +40,4 @@
 
 USER user
 ENV HOME /home/user
-CMD /opt/start.sh
```

### README.md

```
@@ -3,41 +3,36 @@
 Контейнер на базе ubuntu 14.04, в составе infinality-патчи (рендеринг шрифтов),
 шрифт "Fira Code", набор стандартных шрифтов mscorefonts (читайте лицензию в архиве),
 набор тем Zukitwo, UltraFlatIcons, YltraIcons
-можно не использовать: выкинуть файлы "ultraflat-icons.zip, yltra-icons.zip, zukitwo-themes.zip" ,
-и удалить из DockerFile строки:
-<blockquote>
-  <p>
-  && unzip /opt/zukitwo-themes.zip -d /usr/share/themes \ <br>
-  && unzip /opt/yltra-icons.zip -d /usr/share/icons \ <br>
-  && unzip /opt/ultraflat-icons.zip -d /usr/share/icons \ <br>
-  </p>
-</blockquote>
 
-В репозитории присутствует light-версия Dockerfile (без тем, шрифтов, infinality, только mscorefonts)
-находится в директории ./light
 
 ### Настройка, сборка и использование
 
 * В Dockerfile исправить версию платформы, архитектуру и используемую кодировку
-<blockquote>
-  ENV PLT_VERSION 8.3.7-1873 <br>
-  ENV PLT_ARCH amd64 <br>
+```
+  ENV PLAT_VERSION 8.3.7-1873 <br>
+  ENV PLAT_ARCH amd64 <br>
   ENV LANG ru_RU.utf8 <br>
-</blockquote>
+```
 
-* Разместить официальные deb-пакеты 1С соответствующих версий и архитектуры (common, server, client) в директории ./dist (server нужен тоже, не знаю зачем, но он указан в зависимостях пакета client (1С -- такая 1С))
+* Разместить официальные deb-пакеты 1С соответствующих версий и архитектуры (common, server, client) в директории ./dist 
 
-* Собрать контейнер `docker build -t psyriccio/dck1c .`
+* Собрать контейнер (где 1469 ветка платформы):
 
-* Использовать для запуска `./run.sh` или комманду вида
-`docker run -t --rm -e DISPLAY -v $HOME/.Xauthority:/home/user/.Xauthority -v $HOME:/home/user -v /mnt:/mnt --net=host --pid=host --ipc=host psyriccio/dck1c`
+ ```docker build -t ep4sh/1c-8312:1469 .```
 
-![imlst.png](./screenshots/imglst.png)
+* Использовать для запуска ```./run.sh``` или комманду вида
 
-![shot01.png](./screenshots/shot01.png)
+```docker run --rm -d -t --name=1c -e DISPLAY -v $HOME/.Xauthority:/home/user/.Xauthority -v $HOME:/home/user -v /mnt/docker:/mnt --net=host --pid=host --ipc=host ep4sh/1c-8312:1469```
 
-![shot02.png](./screenshots/shot02.png)
+* Вывалить юзеру ярлык на рабочий стол (`run.sh`):
 
-![shot03.png](./screenshots/shot03.png)
+```
+#!/bin/bash
+docker exec -it  ep4sh/1c-8312:1469 /opt/1C/v8.3/x86_64/1cv8 &> /dev/null &
+```
 
-Лицензия: [LGPLv3](./LICENSE.TXT)
+Авторство: [ep4sh2k](https://github.com/ep4sh)  
+            Оригинальный репозиторий принадлежит [psyriccio](https://github.com/psyriccio).   
+            И за это ему огромное **спасибо**. 
+            Документированные изменения: [changes](./CHANGES) 
+Лицензия: [GNU LGPL](./LICENCE)
```

